<?php

/**
 * FAE 
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2020 Callum Smith
 */

namespace FAE\rest;

use Symfony\Component\HttpFoundation\Request;

class restActionSchemaCreate extends restActionSchema
{

  /**
   * Action rest create
   *
   * @param Request $this->request
   * @throws restActionException
   * @return object
   */
  public function action(): object
  {

    $filter = (is_array($this->request->request->get('filter')) ? array_merge($this->request->request->get('filter'), $this->routeVariables) : $this->routeVariables);
    $this->request->request->remove('filter');

    $response = $this->dataInstance->insert($this->request->request->all(), $filter);

    if ($response === false) {
      throw new restActionException('An unknown error occured when creating the object');
    }
    
    return (object) ['data' => $response, 'success' => true];
  }
}
