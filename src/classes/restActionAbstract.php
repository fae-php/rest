<?php

/**
 * FAE 
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2020 Callum Smith
 */

namespace FAE\rest;

use FAE\auth\authException;
use FAE\permissions\permException;
use Symfony\Component\HttpFoundation\Request;

abstract class restActionAbstract
{
  // @var Request
  var $request;
  // @var object Response object from the action
  var $response;
  // @var bool Handle exceptions automatically, if true this will output formatted errors if one occurs
  var $handleExceptions = true;
  // @var string Return format
  var $returnFormat = 'application/json';
  // @var array Route variables passed from the Symfony route
  var $routeVariables;

  public function __construct(array $routeVariables)
  {
    $this->routeVariables = $routeVariables;
  }

  protected function formatData($data, int $count = 0, int $start = 0, int $length = 0): object
  {
    $output = new \StdClass();
    $output->data = new \StdClass();

    $output->data->startIndex     = $start;
    $output->data->totalItems     = $count ? $count : count($data);
    $output->data->itemsPerPage   = $length ? $length : $count;
    $output->data->pageIndex      = $length ? floor($start / $length) : 0;
    $output->data->totalPages     = $length ? ceil(($count ? $count : count($data)) / $length) : 1;
    $output->data->items          = $data;

    return $output;
  }

  public function setRequest(Request $request): self
  {
    $this->request = $request;
    return $this;
  }

  public function handleExceptions(bool $handle = true): self
  {
    $this->handleExceptions = $handle;
    return $this;
  }

  public function setReturnFormat(string $format = 'application/json'): self
  {
    $this->returnFormat = $format;
    return $this;
  }

  protected function parseRequest(): self
  {
    if (!$this->request) {
      throw new restActionException('Request not defined');
    }
    if (0 === strpos($this->request->headers->get('CONTENT_TYPE')??'', 'application/json')) {
      $data = json_decode($this->request->getContent(), true);
      if ($this->request->getMethod() === 'GET') {
        $this->request = $this->request->duplicate($data);
      } elseif (in_array($this->request->getMethod(), ['POST', 'PUT', 'PATCH', 'DELETE'])) {
        $this->request = $this->request->duplicate([], $data);
      }
    }
    return $this;
  }

  public function outputAuthorizationError(permException $e): void
  {
    $this->errorDisplay($e, 403);
  }

  public function outputAuthenticationError(authException $e): void
  {
    header("WWW-Authenticate: Bearer realm='API Access'");
    $this->errorDisplay($e, 401);
  }

  public function errorDisplay(\Exception $e, int $code = 500): void
  {
    $this->outputHeaders();
    http_response_code($code);
    $errorDisplay = ['error' => $e->getMessage(), 'error_code' => $code];
    if(defined('DEBUG') && constant('DEBUG') == true){
      $errorDisplay['stack_trace'] = $e->getTrace();
    }
    echo json_encode((object) $errorDisplay);
    exit;
  }

  public function outputHeaders(): self
  {
    header('Content-Type: ' . $this->returnFormat);
    http_response_code(200);
    return $this;
  }

  public function output(): void
  {
    $this->outputHeaders();
    switch ($this->returnFormat) {
      case 'application/json':
        echo json_encode($this->response);
        break;
      default:
        throw new restActionException("Could not handle return format requested");
    }
  }

  public function __invoke(Request $request): restActionAbstract
  {
    $this->setRequest($request)
      ->parseRequest($request);
    if ($this->handleExceptions) {
      try {
        $this->response = $this->action();
      } catch (authException $e) {
        $this->outputAuthenticationError($e);
      } catch (permException $e) {
        $this->outputAuthorizationError($e);
      } catch (\Exception $e) {
        $this->errorDisplay($e);
      }
    } else {
      $this->response = $this->action();
    }
    return $this;
  }

  abstract public function action(): object;
}
