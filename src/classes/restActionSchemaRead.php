<?php

/**
 * FAE 
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2020 Callum Smith
 */

namespace FAE\rest;

use Symfony\Component\HttpFoundation\Request;

class restActionSchemaRead extends restActionSchema
{

  public function action(): object
  {
    if (array_key_exists('id', $this->routeVariables)) {
      $this->request->query->set('filter', array_merge($this->request->query->all(), ['id' => $this->routeVariables['id']]));
    }

    $filter   = (array) $this->request->query->get('filter');
    $count    = (int) $this->dataInstance->count($filter);
    $start    = (int) $this->request->query->getInt('start');
    $length   = (int) $this->request->query->getInt('length', 10);
    $order     = (array) $this->request->query->get('order', []);
    $query    = $this->dataInstance->get($filter, [], $start, $length, $order);
    $result   = $query->fetchAll();

    $result = $this->dataInstance->parseDataset($result);

    if (!empty($_GET['childdata'])) {
      $result = $this->dataInstance->expandDataset($result);
    }

    return $this->formatData($result, $count, $start, $length);
  }
}
