<?php

/**
 * FAE 
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2020 Callum Smith
 */

namespace FAE\rest;

use Symfony\Component\HttpFoundation\Request;

class restActionSchemaUpdate extends restActionSchema
{

  /**
   * Action rest update
   *
   * @param Request $this->request
   * @throws restActionException
   * @return object
   */
  public function action(): object
  {
    $filter = (is_array($this->request->request->get('filter')) ? array_merge($this->request->request->get('filter'), $this->routeVariables) : $this->routeVariables);
    $this->request->request->remove('filter');

    // Handle stateless patch/put configuration
    if ($this->request->isMethod('PUT')) {
      $this->request->request->set('_stateless', true);
    } elseif ($this->request->isMethod('PATCH')) {
      $this->request->request->set('_stateless', false);
    }

    $results = $this->dataInstance->update($this->request->request->all(), $filter);

    if ($results === false) {
      throw new restActionException('An unknown error occured when updating the object');
    }
    
    return (object) ['data' => $results, 'success' => true];
  }
}
