<?php

/**
 * FAE 
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2020 Callum Smith
 */

namespace FAE\rest;

use Symfony\Component\HttpFoundation\Request;

abstract class restActionSchema extends restActionAbstract 
{
  // @var string Class name reference for the data
  var $dataClass;
  var $dataInstance;
  var $run = true;
  var $data = [];

  public function __construct(string $dataClass, array $routeVariables = [])
  {
    $this->dataClass = $dataClass;
    $this->routeVariables = $routeVariables;
    $this->dataInstance = new $dataClass('default', $routeVariables);
    // Disable for classes with rest disabled
    if ($this->dataInstance->_rest === false) {
      $this->run = false;
    }
  }

  public function __invoke(Request $request): restActionAbstract
  {
    if(!$this->run){
      return $this;
    }
    return parent::__invoke($request);
  }
}