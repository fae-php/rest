<?php

namespace FAE\rest;

use FAE\auth\authException;
use FAE\permissions\permException;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

use \StdClass;
use Symfony\Component\HttpFoundation\Request;

class rest
{

  // @var RouteCollection $_routes
  static $_routes;

  /**
   * action request
   *
   * @param array $variables
   * @deprecated v0.2.3 Use {@see \FAE\rest\restActionAbstract} for a better implementation of rest actions
   * @return void
   */
  static function action(array $variables)
  {
    $class = $variables['_dataController'];
    $method = $variables['_action'];
    $args = [];
    $data = new StdClass();

    if (is_array($variables['_args']) && count($variables['_args'])) {
      foreach ($variables['_args'] as $key) {
        if (array_key_exists($key, $_REQUEST)) {
          $args[] = $_REQUEST[$key] == '' ? null : $_REQUEST[$key];
        }
      }
    }

    try {
      self::actionMethod($class, $method, $args);
      $data->success = true;
    } catch (\Exception $e) {
      self::errorDisplay($e);
    }

    self::output($data);
  }


  /**
   * action method
   *
   * @param string $class Class name to call
   * @param string $method Method name to call
   * @param array $args
   * @deprecated v0.2.3 Use {@see \FAE\rest\restActionAbstract} for a better implementation of rest actions
   * @return void
   */
  static function actionMethod(string $class, string $method, array $args)
  {
    if (!class_exists($class)) {
      throw new \Exception('Action class does not exist');
    }
    $instance = new $class;
    if (!method_exists($instance, $method)) {
      throw new \Exception('Called method does not exist');
    }

    return $instance->$method(...$args);
  }

  /**
   * Routeloader function
   *
   * @return RouteCollection
   */
  static function routes(): RouteCollection
  {
    global $config;

    if (!self::$_routes) {
      self::$_routes = new RouteCollection();
    }

    if (class_exists('\\FAE\\schema\\model\\schema')) {
      $schema = new \FAE\schema\model\schema();
      $schema->loadSchema();

      $route = new Route(
        '/schema',
        ['_controller' => self::class, '_action' => restActionSchemaLoader::class],
        [],
        [],
        '',
        [],
        ['GET']
      );
      self::$_routes->add('rest-schema', $route);

      foreach ($schema->_models as $model) {
        $modelClass = ($model->class ? $model->class : '\\FAE\\' . $model->frame . '\\' . $model->table);
        $routable = str_replace(['_'], '-', $model->table);
        if (!class_exists($modelClass)) {
          trigger_error("Could not load model class {$modelClass}", E_USER_WARNING);
          continue;
        }
        try {
          $instance = new $modelClass();
          if (!isset($instance->_rest) || $instance->_rest !== true) {
            continue;
          }
        } catch (\Exception $e) {
          // softfail
        }
        $route = new Route(
          '/schema/' . $routable,
          ['_controller' => self::class, '_action' => restActionSchemaLoader::class, '_table' => $routable],
          [],
          [],
          '',
          [],
          ['GET']
        );
        self::$_routes->add('rest-' . $routable . '-S', $route);
        $route = new Route(
          '/' . $routable,
          ['_controller' => self::class, '_dataController' => $modelClass, '_method' => $routable, '_action' => 'schema_create'],
          [],
          [],
          '',
          [],
          ['POST']
        );
        self::$_routes->add('rest-' . $routable . '-C', $route);
        $route = new Route(
          '/' . $routable,
          ['_controller' => self::class, '_dataController' => $modelClass, '_method' => $routable, '_action' => 'schema_read'],
          [],
          [],
          '',
          [],
          ['GET']
        );
        self::$_routes->add('rest-' . $routable . '-R', $route);
        $route = new Route(
          '/' . $routable . '/{id}',
          ['_controller' => self::class, '_dataController' => $modelClass, '_method' => $routable, '_action' => 'schema_read'],
          ['id' => '([1-9]+[0-9]*|[a-f0-9]{8}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{12})'],
          [],
          '',
          [],
          ['GET']
        );
        self::$_routes->add('rest-' . $routable . '-RO', $route);
        $route = new Route(
          '/' . $routable . '/{id}',
          ['_controller' => self::class, '_dataController' => $modelClass, '_method' => $routable, '_action' => 'schema_update'],
          ['id' => '([1-9]+[0-9]*|[a-f0-9]{8}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{12})'],
          [],
          '',
          [],
          ['POST', 'PUT', 'PATCH']
        );
        self::$_routes->add('rest-' . $routable . '-U', $route);
        $route = new Route(
          '/' . $routable . '/{id}',
          ['_controller' => self::class, '_dataController' => $modelClass, '_method' => $routable, '_action' => 'schema_delete'],
          ['id' => '([1-9]+[0-9]*|[a-f0-9]{8}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{12})'],
          [],
          '',
          [],
          ['DELETE']
        );
        self::$_routes->add('rest-' . $routable . '-D', $route);
      }
    }

    self::$_routes->addPrefix("/api/{$config->apiVersion}");
    self::$_routes->setSchemes(['https', 'http']);

    return self::$_routes;
  }

  /**
   * Format data for REST output with page data
   *
   * @param array|object $data Input
   * @param integer $count Total number of data objects - will default to count($data)
   * @param integer $start Starting zero-index number of the represented dataset
   * @param integer $length Length of the page of data represented
   * @return object Formatted data object
   * @deprecated v0.2.3 Use {@see \FAE\rest\restActionAbstract} for a better implementation of rest outputs
   */
  static function formatData($data, int $count = 0, int $start = 0, int $length = 0): object
  {
    $output = new StdClass();
    $output->data = new StdClass();

    $output->data->startIndex     = $start;
    $output->data->totalItems     = $count ? $count : count($data);
    $output->data->itemsPerPage   = $length ? $length : $count;
    $output->data->pageIndex      = $length ? floor($start / $length) : 0;
    $output->data->totalPages     = $length ? ceil(($count ? $count : count($data)) / $length) : 1;
    $output->data->items          = $data;

    return $output;
  }

  /**
   * Display authorization error
   *
   * @param permException $e
   * @deprecated v0.2.3 Use {@see \FAE\rest\restActionAbstract} for a better implementation of rest outputs
   * @return void
   */
  public static function authorizationError(permException $e)
  {
    self::errorDisplay($e, 403);
  }

  /**
   * Display authentication error
   *
   * @param authException $e
   * @deprecated v0.2.3 Use {@see \FAE\rest\restActionAbstract} for a better implementation of rest outputs
   * @return void
   */
  public static function authenticationError(authException $e)
  {
    header("WWW-Authenticate: Bearer realm='API Access'");
    self::errorDisplay($e, 401);
  }

  /**
   * Handle error display
   *
   * @param \Exception $message
   * @param integer $code
   * @deprecated v0.2.3 Use {@see \FAE\rest\restActionAbstract} for a better implementation of rest outputs
   * @return void
   */
  public static function errorDisplay(\Exception $message, int $code = 500)
  {
    http_response_code($code);
    echo rest::output((object) ['error' => $message->getMessage(), 'error_code' => $code]);
    exit;
  }

  /**
   * Output data as JSON with headers
   *
   * @param object|array $data
   * @deprecated v0.2.3 Use {@see \FAE\rest\restActionAbstract} for a better implementation of rest outputs
   * @return void
   */
  public static function output($data)
  {
    header('Content-Type: application/json');
    echo json_encode($data);
  }

  /**
   * Invoke the rest API handler
   * 
   * By default this 
   * schema_ actions are auto-generated from models defined using the FAE\schema module if it is installed.
   *
   * @param array $variables
   * @return void
   */
  public function __invoke(array $variables)
  {
    $request = Request::createFromGlobals();

    switch ($variables['_action']) {
      
      case 'schema_read':

        $restInstance = new restActionSchemaRead($variables['_dataController'], $variables);
        $restInstance($request)->output();

        break;

      case 'schema_create':

        $restInstance = new restActionSchemaCreate($variables['_dataController'], $variables);
        $restInstance($request)->output();

        break;

      case 'schema_update':

        $restInstance = new restActionSchemaUpdate($variables['_dataController'], $variables);
        $restInstance($request)->output();

        break;

      case 'schema_delete':

        $restInstance = new restActionSchemaDelete($variables['_dataController'], $variables);
        $restInstance($request)->output();

        break;

      default:

        if (class_exists($variables['_action'])) {
          $class = new $variables['_action']($variables);
          if($class instanceof restActionAbstract){
            // Invoke method and output it
            $class($request)->output();
          } else {
            // Invoke method directly - the class must know how to output if it is not an instance of restActionAbstract
            call_user_func($class, $variables);
          }
        } elseif(function_exists($variables['_action'])) {
          call_user_func($variables['_action'], $variables);
        }

        break;
    }
  }
}
