<?php

/**
 * FAE 
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2020 Callum Smith
 */

namespace FAE\rest\events;

class restSchemaRenderPreHook extends restSchemaRenderHook
{
  public const NAME = 'rest.schema.render.prehook';
}
