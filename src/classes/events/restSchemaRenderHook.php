<?php

/**
 * FAE 
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2020 Callum Smith
 */

 namespace FAE\rest\events;

use stdClass;
use Symfony\Contracts\EventDispatcher\Event;

abstract class restSchemaRenderHook extends Event
 {
  // @var stdClass $schema object to output a schema
  var $schema;

  public function __construct(stdClass $schema)
  {
      $this->schema = $schema;
  }

  public function getSchema(): stdClass
  {
    return $this->schema;
  }

  public function setSchema(stdClass $schema): void
  {
    $this->schema = $schema;
  }
 }