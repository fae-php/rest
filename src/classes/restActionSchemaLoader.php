<?php

/**
 * FAE 
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2020 Callum Smith
 */

namespace FAE\rest;

use FAE\event_handler\eventHandler;
use FAE\rest\events\restSchemaRenderPostHook;
use FAE\rest\events\restSchemaRenderPreHook;
use FAE\schema\model\schema;
use stdClass;

class restActionSchemaLoader extends restActionAbstract
{

  // @var string|false Load a specific table or the whole schema
  var $loadTable = false;

  // @var schema Schema reference
  var $schema;

  // @var array Cache of which models are included and which are not
  var $includeModelCache = [];

  public function action(): object
  {
    global $config;

    $this->loadTable = $this->routeVariables['_table'] ?? false;

    $this->schema = new schema();
    $this->schema->loadSchema();

    $output = new stdClass();

    $output->schema = $config->namespace;

    $dispatcher = eventHandler::getHandler()->getDispatcher();
    $preSchemaEvent = new restSchemaRenderPreHook($output);
    $dispatcher->dispatch($preSchemaEvent, restSchemaRenderPreHook::NAME);
    $output = $preSchemaEvent->getSchema();

    if ($this->loadTable) {
      $output->data = new stdClass();
      $output->data->items[] = $this->schema->loadModel($this->loadTable);
      $output->endpoint = BASE_URL . "/api/{$config->apiVersion}";
      return $output;
    }

    $models = $this->loadSchema();

    $output->data = new stdClass();
    $output->data->items = $models;
    $output->endpoint = BASE_URL . "/api/{$config->apiVersion}";

    $postSchemaEvent = new restSchemaRenderPostHook($output);
    $dispatcher->dispatch($postSchemaEvent, restSchemaRenderPostHook::NAME);
    $output = $postSchemaEvent->getSchema();

    return $output;
  }

  private function loadSchema(): array
  {
    global $config;

    $models = [];

    foreach ($this->schema->_models as $name => $model) {
      $class = $model->class;
      $instance = new $class();
      $model->endpoint = BASE_URL . "/api/{$config->apiVersion}/{$name}";
      if ($this->includeModel($model)) {
        $model = clone ($model);
        if (property_exists($model, 'childrenList')) {
          foreach ($model->childrenList as $k => $child) {
            if (!$this->includeModel($child)) {
              unset($model->childrenList[$k]);
            }
          }
          $model->childrenList = array_values((array) $model->childrenList);
          if(!count($model->childrenList)){
            unset($model->childrenList);
          }
        }
        if (property_exists($model, 'children')) {
          foreach ($model->children as $k => $child) {
            if (!$this->includeModel($child['table'])) {
              unset($model->children[$k]);
            }
          }
          $model->children = array_values((array) $model->children);
          if(!count($model->children)){
            unset($model->children);
          }
        }
        if (property_exists($model, 'parents')) {
          foreach ($model->parents as $k => $parent) {
            if (!$this->includeModel($parent)) {
              unset($model->parents[$k]);
            }
          }
          $model->parents = array_values((array) $model->parents);
          if(!count($model->parents)){
            unset($model->parents);
          }
        }
        $models[$name] = $model;
      }
    }

    return $models;
  }

  private function includeModel($model): bool
  {
    if (is_string($model)) {
      if (array_key_exists($model, $this->includeModelCache)) {
        return $this->includeModelCache[$model];
      }
      $class = $this->schema->loadModel($model)->class;
      $instance = new $class();
      if (isset($instance->_rest) && $instance->_rest === true) {
        $this->includeModelCache[$model] = true;
        return true;
      } else {
        $this->includeModelCache[$model] = false;
      }
    } elseif (is_object($model) && property_exists($model, 'class')) {
      if (array_key_exists($model->table, $this->includeModelCache)) {
        return $this->includeModelCache[$model->table];
      }
      $class = $model->class;
      $instance = new $class();
      if (isset($instance->_rest) && $instance->_rest === true) {
        $this->includeModelCache[$model->table] = true;
        return true;
      } else {
        $this->includeModelCache[$model->table] = false;
      }
    }
    return false;
  }
}
