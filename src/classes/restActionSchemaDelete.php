<?php

/**
 * FAE 
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2020 Callum Smith
 */

namespace FAE\rest;

use Symfony\Component\HttpFoundation\Request;

class restActionSchemaDelete extends restActionSchema
{

  /**
   * Action rest delete
   *
   * @param Request $this->request
   * @throws restActionException
   * @return object
   */
  public function action(): object
  {
    if (array_key_exists('id', $this->routeVariables)) {
      $this->request->request->set('id', $this->routeVariables['id']);
    }
    
    $results = $this->dataInstance->delete(['id' => $this->request->request->get('id')]);

    if ($results === false) {
      throw new restActionException('An unknown error occured when deleting the object');
    }
    
    return (object) ['success' => true];
  }
}
